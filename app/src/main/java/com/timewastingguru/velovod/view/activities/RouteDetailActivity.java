package com.timewastingguru.velovod.view.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.timewastingguru.velovod.R;
import com.timewastingguru.velovod.databinding.RouteDetailActivityBinding;
import com.timewastingguru.velovod.model.entities.Route;
import com.timewastingguru.velovod.view.adapters.RouteDetailAdapter;

/**
 * Created by artoymtkachenko on 07.07.15.
 */
public class RouteDetailActivity extends AppCompatActivity {

    public static final String ROUTE = "route";

    RouteDetailActivityBinding mBinding;
    private RouteDetailAdapter mAdapter;
    Route mRoute;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.route_detail_activity);
        mRoute = getIntent().getParcelableExtra(ROUTE);
        mBinding = DataBindingUtil.setContentView(this, R.layout.route_detail_activity);
        mBinding.pointsList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RouteDetailAdapter(mRoute);
        mBinding.pointsList.setAdapter(mAdapter);
    }
}
