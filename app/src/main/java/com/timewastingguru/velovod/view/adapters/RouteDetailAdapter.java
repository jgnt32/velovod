package com.timewastingguru.velovod.view.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.timewastingguru.velovod.R;
import com.timewastingguru.velovod.databinding.PointListItemBinding;
import com.timewastingguru.velovod.model.entities.Route;

/**
 * Created by artoymtkachenko on 07.07.15.
 */
public class RouteDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Route mRoute;

    public RouteDetailAdapter(Route route) {
        this.mRoute = route;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new PointViewHolder(inflater.inflate(R.layout.point_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PointViewHolder)holder).mBinding.setPoint(mRoute.getPoints().get(position));
    }

    @Override
    public int getItemCount() {
        return mRoute.getPoints().size();
    }


    public class PointViewHolder extends RecyclerView.ViewHolder {

        PointListItemBinding mBinding;

        public PointViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
