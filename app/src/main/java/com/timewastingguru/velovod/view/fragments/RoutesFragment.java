package com.timewastingguru.velovod.view.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.timewastingguru.velovod.R;
import com.timewastingguru.velovod.databinding.RoutesFragmentBinding;
import com.timewastingguru.velovod.view.activities.RouteDetailActivity;
import com.timewastingguru.velovod.view.adapters.RoutesAdapter;
import com.timewastingguru.velovod.viewmodel.RouteViewModel;
import com.timewastingguru.velovod.viewmodel.RoutesViewModel;

/**
 * Created by artoymtkachenko on 05.07.15.
 */
public class RoutesFragment extends Fragment implements View.OnClickListener {

    private RoutesFragmentBinding mBinding;
    private RoutesViewModel mModel;
    private RoutesAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mModel = new RoutesViewModel();
        mAdapter = new RoutesAdapter(mModel, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.routes_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = DataBindingUtil.bind(view);
        mBinding.routesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBinding.routesList.setAdapter(mAdapter);
        mBinding.setView(this);
        mBinding.setRoutesViewModel(mModel);
    }

    @Override
    public void onClick(View v) {
        int childAdapterPosition = mBinding.routesList.getChildAdapterPosition(v);
        RouteViewModel routeViewModel = mModel.routes.get(childAdapterPosition);
        Intent intent = new Intent(getActivity(), RouteDetailActivity.class);
        intent.putExtra(RouteDetailActivity.ROUTE, routeViewModel.getRoute());
        startActivity(intent);
    }
}
