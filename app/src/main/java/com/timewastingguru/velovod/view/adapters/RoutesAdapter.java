package com.timewastingguru.velovod.view.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.timewastingguru.velovod.R;
import com.timewastingguru.velovod.databinding.RouteListItemBinding;
import com.timewastingguru.velovod.viewmodel.RoutesViewModel;

/**
 * Created by artoymtkachenko on 05.07.15.
 */
public class RoutesAdapter extends RecyclerView.Adapter<RoutesAdapter.RouteHolder> {

    private final RoutesViewModel mViewModel;
    private final View.OnClickListener mOnRouteClickListener;

    public RoutesAdapter(RoutesViewModel viewModel, View.OnClickListener onRouteClickListener) {
        mViewModel = viewModel;
        mOnRouteClickListener = onRouteClickListener;
    }

    @Override
    public RouteHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.route_list_item, parent, false);
        v.setOnClickListener(mOnRouteClickListener);
        return new RouteHolder(v);
    }

    @Override
    public void onBindViewHolder(RouteHolder holder, int position) {
        holder.mBinding.setRoute(mViewModel.routes.get(position));
    }

    @Override
    public int getItemCount() {
        return mViewModel.routes.size();
    }

    class RouteHolder extends RecyclerView.ViewHolder{
        RouteListItemBinding mBinding;
        public RouteHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }

    }

}
