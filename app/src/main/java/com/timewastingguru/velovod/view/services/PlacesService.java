package com.timewastingguru.velovod.view.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.android.gms.location.LocationRequest;
import com.timewastingguru.velovod.model.entities.Point;
import com.timewastingguru.velovod.model.entities.Route;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by artoymtkachenko on 05.07.15.
 */
public class PlacesService extends Service {

    public static final String ROUTE = "route";
    LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setNumUpdates(5)
            .setInterval(100);

    private Observable<Point> mObservable;
    private Route mRoute;
    private Subscription mSubscription;


    private float getDistance(Location location, Point point) {
        float[] results = new float[0];
        Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                point.getLocation().getLat(), point.getLocation().getLon(), results);
        return results[0];
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mRoute = intent.getParcelableExtra(ROUTE);
        mSubscription = mObservable.subscribe(new Action1<Point>() {
            @Override
            public void call(Point point) {
                if (point != null) {
                    point.getAudio(); // Play sounds
                }
            }
        });
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ReactiveLocationProvider locationProvider
                = new ReactiveLocationProvider(getApplicationContext());
        mObservable = locationProvider.getUpdatedLocation(request)
                .map(new Func1<Location, Point>() {
                    @Override
                    public Point call(Location location) {
                        if (mRoute != null) {
                            for (Point point : mRoute.getPoints()) {
                                if (!point.isVisited() && getDistance(location, point) <= point.getRadius()) {
                                    point.setVisited(true);
                                    return point;
                                }
                            }
                        }
                        return null;
                    }
                });
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscription.unsubscribe();
    }
}
