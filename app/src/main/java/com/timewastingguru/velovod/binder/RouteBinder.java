package com.timewastingguru.velovod.binder;

import com.timewastingguru.velovod.viewmodel.RouteViewModel;

import net.droidlabs.mvvm.recyclerview.adapter.binder.ItemBinderBase;

/**
 * Created by artoymtkachenko on 06.07.15.
 */
public class RouteBinder extends ItemBinderBase<RouteViewModel> {

    public RouteBinder(int bindingVariable, int layoutId) {
        super(bindingVariable, layoutId);
    }

}
