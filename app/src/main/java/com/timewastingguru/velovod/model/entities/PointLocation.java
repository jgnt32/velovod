package com.timewastingguru.velovod.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by artoymtkachenko on 04.07.15.
 */
public class PointLocation implements Parcelable {

    public PointLocation(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    double lat;

    double lon;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lon);
    }

    protected PointLocation(Parcel in) {
        this.lat = in.readDouble();
        this.lon = in.readDouble();
    }

    public static final Parcelable.Creator<PointLocation> CREATOR = new Parcelable.Creator<PointLocation>() {
        public PointLocation createFromParcel(Parcel source) {
            return new PointLocation(source);
        }

        public PointLocation[] newArray(int size) {
            return new PointLocation[size];
        }
    };
}
