package com.timewastingguru.velovod.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by artoymtkachenko on 04.07.15.
 */
public class Route implements Parcelable {

    public Route(String title) {
        this.title = title;
    }

    String title;

    List<Point> points;

    int distance;

    int duration;

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeTypedList(points);
    }

    public Route() {
    }

    protected Route(Parcel in) {
        this.title = in.readString();
        this.points = in.createTypedArrayList(Point.CREATOR);
    }

    public static final Parcelable.Creator<Route> CREATOR = new Parcelable.Creator<Route>() {
        public Route createFromParcel(Parcel source) {
            return new Route(source);
        }

        public Route[] newArray(int size) {
            return new Route[size];
        }
    };
}
