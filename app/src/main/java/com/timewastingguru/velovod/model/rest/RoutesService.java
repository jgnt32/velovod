package com.timewastingguru.velovod.model.rest;


import com.timewastingguru.velovod.model.entities.Route;

import java.util.List;

import rx.Observable;

/**
 * Created by artoymtkachenko on 04.07.15.
 */
public interface RoutesService{

    Observable<List<Route>> getRoutes();

    Observable<Route> getRouteDetail();

}
