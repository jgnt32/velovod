package com.timewastingguru.velovod.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by artoymtkachenko on 04.07.15.
 */
public class Point implements Parcelable {

    public static final int DEFAULT_RADIUS = 20;

    String title;
    int radius;
    PointLocation location;
    boolean visited;
    String audio;
    String image;

    public Point(String title, PointLocation location, String audio) {
        this(title, DEFAULT_RADIUS, location, audio);
    }

    public Point(String title, int radius, PointLocation location, String audio) {
        this.title = title;
        this.radius = radius;
        this.location = location;
        this.audio = audio;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PointLocation getLocation() {
        return location;
    }

    public void setLocation(PointLocation location) {
        this.location = location;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeInt(this.radius);
        dest.writeParcelable(this.location, 0);
        dest.writeByte(visited ? (byte) 1 : (byte) 0);
        dest.writeString(this.audio);
        dest.writeString(this.image);
    }

    protected Point(Parcel in) {
        this.title = in.readString();
        this.radius = in.readInt();
        this.location = in.readParcelable(PointLocation.class.getClassLoader());
        this.visited = in.readByte() != 0;
        this.audio = in.readString();
        this.image = in.readString();
    }

    public static final Creator<Point> CREATOR = new Creator<Point>() {
        public Point createFromParcel(Parcel source) {
            return new Point(source);
        }

        public Point[] newArray(int size) {
            return new Point[size];
        }
    };
}
