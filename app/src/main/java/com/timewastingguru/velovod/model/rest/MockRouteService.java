package com.timewastingguru.velovod.model.rest;

import com.timewastingguru.velovod.model.entities.Point;
import com.timewastingguru.velovod.model.entities.PointLocation;
import com.timewastingguru.velovod.model.entities.Route;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by artoymtkachenko on 05.07.15.
 */
public class MockRouteService implements RoutesService {
    
    @Override
    public Observable<List<Route>> getRoutes() {
        return Observable.create(new Observable.OnSubscribe<List<Route>>() {
            @Override
            public void call(Subscriber<? super List<Route>> subscriber) {

                List<Route> routes = new ArrayList<Route>();
                Route seafront = new Route();
                seafront.setTitle("Набережная");
                List<Point> points = new ArrayList<Point>();
                points.add(new Point("Бронзовый таможенник", new PointLocation(47.212778, 39.706389), ""));
                points.add(new Point("Купание коня", new PointLocation(47.2125, 39.706944), ""));
                points.add(new Point("Чугунные кнехты", new PointLocation(47.212778, 39.708611), ""));
                seafront.setPoints(points);

                routes.add(seafront);

                Route center = new Route();
                seafront.setTitle("Центральный маршрут");
                List<Point> centerPoints = new ArrayList<Point>();
                centerPoints.add(new Point("CreativeSpace.PRO", new PointLocation(47.212778, 39.706389), ""));
                centerPoints.add(new Point("Покровский сквер", new PointLocation(47.2125, 39.706944), ""));
                centerPoints.add(new Point("Храм Покрова Пресвятой Богородицы", new PointLocation(47.212778, 39.708611), ""));
                center.setPoints(points);

                subscriber.onNext(routes);
                subscriber.onCompleted();
            }
        }).compose(this.<List<Route>>applyDefaultConfiguration());
    }

    @Override
    public Observable<Route> getRouteDetail() {
        return null;
    }

    private <T> Observable.Transformer<T, T> applyDefaultConfiguration() {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> tObservable) {
                return tObservable
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());
//                        .retry(3)
//                        .timeout(10, TimeUnit.SECONDS);
            }
        };
    }
}
