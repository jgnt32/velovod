package com.timewastingguru.velovod.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;

import com.timewastingguru.velovod.model.entities.Point;
import com.timewastingguru.velovod.model.entities.Route;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artoymtkachenko on 05.07.15.
 */
public class RoutesViewModel extends BaseObservable {

    @Bindable
    public final ObservableArrayList<RouteViewModel> routes = new ObservableArrayList<>();

    public RoutesViewModel() {
        Route route = new Route("Route Test");
        List<Point> points = new ArrayList<>(5);
        points.add(new Point("Point 1", null, null));
        points.add(new Point("Point 2", null, null));
        points.add(new Point("Point 3", null, null));
        points.add(new Point("Point 4", null, null));
        points.add(new Point("Point 5", null, null));
        route.setPoints(points);

        routes.add(new RouteViewModel(route));
        routes.add(new RouteViewModel(route));
        routes.add(new RouteViewModel(route));
        routes.add(new RouteViewModel(route));
    }

    public void addRoutes(List<RouteViewModel> routes){
        this.routes.addAll(routes);
    }
    public void addRoute(RouteViewModel route){
        this.routes.add(route);
    }

}
