package com.timewastingguru.velovod.viewmodel;

import android.databinding.BaseObservable;

import com.timewastingguru.velovod.model.entities.Route;

/**
 * Created by artoymtkachenko on 06.07.15.
 */
public class RouteViewModel extends BaseObservable {

    public final Route route;

    public RouteViewModel(Route route) {
        this.route = route;
    }

    public Route getRoute() {
        return route;
    }

    public String getRouteTitle(){
        return route.getTitle();
    }
}
